<?php
    session_start();
    
    require_once('business/usuarioCrud.php');
    $msg = '';

    if(isset($_POST['usuario'])){
        $user = strip_tags(filter_input(INPUT_POST, 'usuario', FILTER_SANITIZE_STRING));
        $pass = strip_tags(filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING));

        try{
            $resultado = chamaValidacao($user, $pass);

            $_SESSION['tipo'] = $resultado['nivel'];
            header('Location: direction.php');
            
        }catch(Exception $ex){
            $msg = $ex->getMessage();
        }
    }

    require_once(CWU . 'gui/login.php');
?>