
/**********CADASTRO DE ANIMAIS**********/
var form = document.getElementById("form-cadastro-animal");
var obj;
function enviaCadastroAnimal(){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                document.getElementById('resposta-conteudo').innerHTML = obj.responseText;
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasNoSolid/business/animalCrud.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("cadastra=true&nome=" + form.nome.value + "&tipo=" + form.tipo.value + "&tamanho=" + form.comprimento.value + "&peso=" + form.peso.value);
    }catch(e){
        alert("Erro: " + e.message);
    }
}


/**********CADASTRO DE USUÁRIOS**********/
var formUser = document.getElementById("form-cadastro-usuario");
function enviaCadastroUsuario(){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                document.getElementById("resposta-conteudo").innerHTML = obj.responseText;
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasNoSolid/business/usuarioCrud.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("cadastro=true&nome=" + formUser.nome.value + "&usuario=" + formUser.usuario.value + "&senha=" + formUser.senha.value + "&tipo=" + formUser.tipo.value)
    }catch(e){
        alert("Erro: " + e.message);
    }
}



/**********EXCLUI ANIMAL**********/
function excluirAnimal(id){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                alert(obj.responseText);
                location.reload();
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasNoSolid/business/animalCrud.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("excluir=true&&id=" + id);
    }catch(e){
        alert("Erro: " + e.message);
    }
}



/**********ALTERA CADASTRO ANIMAL**********/
var formAlt = document.getElementById("form-altera-animal");
function alteraAnimal(){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                document.getElementById('resposta-conteudo').innerHTML = obj.responseText;
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasNoSolid/business/animalCrud.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("altera=true&id=" + formAlt.id.value + "&nome=" + formAlt.nome.value + "&tipo=" + formAlt.tipo.value + "&tamanho=" + formAlt.comprimento.value + "&peso=" + formAlt.peso.value);
    }catch(e){
        alert("Erro: " + e.message);
    }
}



/**********EXCLUI USUÁRIO**********/
function excluirUsuario(user){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                alert(obj.responseText);
                location.reload();
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasNoSolid/business/usuarioCrud.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("excluir=true&user=" + user);
    }catch(e){
        alert("Erro: " + e.message);
    }
}



/**********ALTERA CADASTRO USUÁRIO**********/
var formAltUser = document.getElementById("form-altera-usuario");
function alteraUsuario(){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                document.getElementById('resposta-conteudo').innerHTML = obj.responseText;
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasNoSolid/business/usuarioCrud.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("altera=true&usuario=" + formAltUser.usuario.value+ "&tipo=" + formAltUser.tipo.value + "&senha=" + formAltUser.senha.value);
    }catch(e){
        alert("Erro: " + e.message);
    }
}



/**********CADASTRO DE CUIDADORES**********/
var formCuidador = document.getElementById("form-cadastro-cuidador");
function enviaCadastroCuidador(){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                document.getElementById("resposta-conteudo").innerHTML = obj.responseText;
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasNoSolid/business/cuidadorCrud.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("cadastro=true&nome=" + formCuidador.nome.value + "&cpf=" + formCuidador.cpf.value + "&responsavel=" + formCuidador.responsavel.value)
    }catch(e){
        alert("Erro: " + e.message);
    }
}



/**********EXCLUI CUIDADOR**********/
function excluirCuidador(id){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                alert(obj.responseText);
                location.reload();
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasNoSolid/business/cuidadorCrud.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("excluir=true&id=" + id);
    }catch(e){
        alert("Erro: " + e.message);
    }
}




/**********ALTERAR CUIDADORES**********/
var formAlteraCuidador = document.getElementById("form-altera-cuidador");
function alteraCuidador(){
    try{
        obj = new XMLHttpRequest();
        obj.onreadystatechange = function(){
            if(obj.status == 200 && obj.readyState == 4){
                document.getElementById("resposta-conteudo").innerHTML = obj.responseText;
            }
        }
        obj.open("POST", "http://localhost/TreinamentoNewM/zoologicoCamadasNoSolid/business/cuidadorCrud.php");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.send("altera=true&id=" + formAlteraCuidador.id.value + "&responsavel=" + formAlteraCuidador.responsavel.value)
    }catch(e){
        alert("Erro: " + e.message);
    }
}