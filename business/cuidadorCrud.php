<?php
    define('CWC', 'C:/xampp/htdocs/TreinamentoNewM/zoologicoCamadasNoSolid/');
    require_once(CWC . 'dal/CuidadorDal.php');
    require_once('CuidadorBusiness.php');
    require_once(CWC . 'entity/Cuidador.php');

    $cuidadorDal = new CuidadorDal();
    $cuidadorBusiness = new CuidadorBusiness();
    
    if(isset($_POST['cadastro'])){
        $cuidador = new Cuidador();
        $cuidador->setNome(strip_tags(filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING)));
        $cpf = strip_tags(filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_STRING));
        $cpf = str_replace('.', '', $cpf);
        $cpf = str_replace('-', '', $cpf);
        $cuidador->setCPF($cpf);
        $cuidador->setResponsavel(strip_tags(filter_input(INPUT_POST, 'responsavel', FILTER_SANITIZE_STRING)));

        try{
            $cuidadorBusiness->cadastro($cuidador);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }
    }

    if(isset($_POST['altera'])){
        $cuidador = new Cuidador();
        $cuidador->setId(strip_tags(filter_input(INPUT_POST, 'id')));
        $cuidador->setResponsavel(strip_tags(filter_input(INPUT_POST, 'responsavel', FILTER_SANITIZE_STRING)));

        try{
            $cuidadorBusiness->alteracao($cuidador);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }

    }

    if(isset($_POST['excluir'])){
        $cuidador = new Cuidador();
        $cuidador->setId(strip_tags(filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING)));
        
        $resultado = $cuidadorDal->excluir($cuidador);

        switch($resultado){
            case 1:
                echo 'Excluido com sucesso';
            break;
            case -1:
                echo 'Erro ao excluir';
            break;
        }
    }

    function chamaListaCuidadores(){
        $cuidadorDal = new CuidadorDal();
        $_REQUEST['cuidadores'] = $cuidadorDal->listaTodos();
    }

?>