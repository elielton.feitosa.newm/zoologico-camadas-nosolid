<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alterar dados do Usuário</title>
    <link rel="stylesheet" href="public/style/geral.css">
    <link rel="stylesheet" href="public/style/cadUsuario.css">
</head>
<body>
    <div class="flex container">
        <header class="flex">
            <a href="?pagina=Home"><img class="logo-zoo" src="public/imagens/logo.png"></a>
            <h2>Alterar dados do Usuário</h2>
            <a href="?pagina=Sair"><img class="img-logout" src="public/imagens/logout.png" alt="Sair"></a>
        </header>
        <main>
            <div class="flex box-dados-animal">
                <form id="form-altera-usuario">
                    <fieldset>
                        <legend>Dados do Usuário</legend>
                        <label for="nome">Nome*</label>
                        <input class="campos-dados" id="campo-nome" type="text" name="nome" value="<?= strip_tags(filter_input(INPUT_GET, 'nome', FILTER_SANITIZE_STRING)) ?>" readonly>
                        <label for="usuario">Usuário*</label>
                        <input class="campos-dados" id="campo-usuario" type="text" name="usuario" value="<?= strip_tags(filter_input(INPUT_GET, 'user', FILTER_SANITIZE_STRING)) ?>" readonly>
                        <label for="senha">Senha*</label>
                        <input class="campos-dados" id="campo-senha" type="text" name="senha">
                        <label for="tipo">Tipo*</label>
                        <select class="campos-dados" id="campo-tipo" name="tipo">
                            <option value="1" <?php if(strip_tags(filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING)) == '1'){echo 'selected';}else{echo '';} ?>>Comum</option>
                            <option value="2" <?php if(strip_tags(filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING)) == '2'){echo 'selected';}else{echo '';} ?>>Administrador</option>
                        </select>
                        <input class="btn-form btn-cadastra" type="button" onClick="alteraUsuario()" Value="Cadastrar">
                        <a href="?pagina=Home"><input class="btn-form btn-voltar" type="button" value="Voltar"></a>
                        <div class="flex resposta-cadastro"><p id="resposta-conteudo"></p></div>
                    </fieldset>
                </form>
            </div>
        </main>
    </div>
    <script type="text/javascript" src="public/js/ajax.js"></script>
</body>
</html>