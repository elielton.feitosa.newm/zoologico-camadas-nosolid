<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/style/geral.css">
    <link rel="stylesheet" href="public/style/login.css">
    <title>Login</title>
</head>
<body>
    <div class="flex container">
        <div class="flex box-side" id="side-1">
            <div class="box-img">
                <img src="public/imagens/img-login.png">
            </div>
        </div>
        <div class="flex box-side" id="side-2">
            <div class="flex login-header">
                <h1>LOGIN</h1>
            </div>
            <div class="flex box-formulario">
                <form id="form-login" method="POST">
                    <input class="campos-dados dados-login" type="text" name="usuario" placeholder="Usuário">
                    <input class="campos-dados dados-login" type="password" name="senha" placeholder="Senha">
                    <input class="campos-dados dados-login" id="btn-login" type="submit" value="Entrar">
                </form>
            </div>
            <div class="resposta"><p id="resposta"><?= $msg ?></p></div>
        </div>
    </div>
</body>
</html>